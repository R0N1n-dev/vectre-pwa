import Vue from "vue";
import { registerSW } from "virtual:pwa-register";
import "spectre.css/dist/spectre-exp.css";
import "spectre.css/dist/spectre-icons.css";
import "spectre.css/dist/spectre.css";
import { VectrePlugin } from "@vectrejs/vectre";
import App from "./App.vue";

const updateSW = registerSW({
  onNeedRefresh() {},
  onOfflineReady() {},
});

Vue.use(VectrePlugin);
new Vue({
  render: (h) => h(App),
}).$mount("#app");
